package adp3;

public class Pascal {
	private int counter;

	public int getCounter() {
		return counter;
	}

	public void reset() {
		counter = 0;
	}

	/**
	 * Rekursive Methode zur Werteberechnung des Pascal'schen Dreiecks
	 * 
	 * @param n
	 *            Zeile des Dreiecks
	 * @param k
	 *            Spalte des Dreiecks
	 * @return liefert die Summe der Werte mit gleichem Index und Index-1 der
	 *         rekursiven Berechnung f�r die darueberliegende Zeile
	 */
	public long rekursiv(long n, long k) {
		// Abbruchbedingungen und die Sonderfaelle
		if (n == 1 || k == 1 || n == k) {
			counter++;
			return 1;
		}
		counter++;
		return rekursiv(n - 1, k - 1) + rekursiv(n - 1, k);
	}

	/**
	 * Iterative Methode zur Zeilenberechnung des Pascal'schen Dreiecks
	 * 
	 * @param n
	 *            Zeile des Dreiecks
	 * @return liefert ein int-Array mit der Zeile an der Position n
	 */
	public long[] iterativ(long n) {
		long[] array = new long[1];
		long[] tmpArray = new long[(int)n];
		int lastIndex = 0;

		for (int i = 0; i < n; i++) {
			counter++;
			array[0] = 1;
			for (int k = 1; k < lastIndex; k++) {
				array[k] = tmpArray[k - 1] + tmpArray[k];
				counter++;
			}
			array[lastIndex] = 1;
			lastIndex++;
			tmpArray = array;
			array = new long[lastIndex + 1];
		}
		array = tmpArray;
		return array;
	}



	public long fast(long n, long k) {
		if (k > n) {
			System.out.println("illegal argument");
		}
		if (k == 0) {
			counter++;
			return 1;
		}
		if (k > n / 2) {
			counter++;
			return fast(n, n - k);
		}
		counter++;
		return n * fast(n - 1, k - 1) / k;
	}

}
