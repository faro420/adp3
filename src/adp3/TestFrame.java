package adp3;

public class TestFrame {

	private static final int N = 60;
	private static long[] array;
	
	public static void main(String[] args) {
		Pascal pascal = new Pascal();
		// TestCase iterativ:
		array = pascal.iterativ(N);
		System.out.println("Ausgabe Zeile "+N+" iterativ:");
		for (int j = 0; j < array.length; j++) {
			System.out.printf("%d  ", array[j]);
		}	
		
		System.out.println("\n"+ pascal.getCounter());
		pascal.reset();
		/*
		 * // TestCase rekursiv:
        System.out.println("Ausgabe Zeile "+N+" rekursiv:");
        for (int k = 1; k <= N; k++) {
            System.out.printf("%d  ", pascal.rekursiv(N, k));       
        }	    		
        
		System.out.println("\n"+ pascal.getCounter());
		pascal.reset();
         */
		// TestCase schnell:
        System.out.println("Ausgabe Zeile "+N+" schnell:");
        for (int k = 0; k < N; k++) {
            System.out.printf("%d  ", pascal.fast(N-1, k));       
        }	
		System.out.println("\n"+ pascal.getCounter());
		pascal.reset();
	}	
}
